# About #

The solution is in script.py. The script will take the .png files from the data directory and produce output.jpeg.

# Dependencies
I used next libraries:

* PIL
* os
* scipy
* numpy 

# How to run it
In order to run it you need to install Anaconda 3 (as  i did) or install Python and all necessary dependancies by yourself. For Anaconda 3 enviroment run command C:\Users\%USER%\Anaconda3\python script.py from the repo directory fo Windows environment.
