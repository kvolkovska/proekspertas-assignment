from PIL import Image
from os import listdir
from os.path import isfile, join
from scipy import ndimage
import numpy as np


def rgb_parameters(w):
    if 380 <= w < 440:
        return -(w - 440.) / (440. - 380.), 0.0, 1.0
    elif 440 <= w < 490:
        return 0.0, (w - 440.) / (490. - 440.), 1.0
    elif 490 <= w < 510:
        return 0.0, 1.0, -(w - 510.) / (510. - 490.)
    elif 510 <= w < 580:
        return (w - 510.) / (580. - 510.), 1.0, 0.0
    elif 580 <= w < 645:
        return 1.0, -(w - 645.) / (645. - 580.), 0.0
    elif 645 <= w <= 780:
        return 1.0, 0.0, 0.0
    else:
        return 0.0, 0.0, 0.0


R = np.zeros((512, 512), 'uint8')
G = np.zeros((512, 512), 'uint8')
B = np.zeros((512, 512), 'uint8')
dir_name = 'data'
spectre = 400
image_file_name_list = sorted([f for f in listdir(dir_name) if isfile(join(dir_name, f))])
for image_file_name in image_file_name_list:
    image = ndimage.imread(join(dir_name, image_file_name))
    r, g, b = rgb_parameters(spectre)
    R = np.maximum(R, (r * image / (2 ** 16) * 256).astype('uint8'))
    G = np.maximum(G, (g * image / (2 ** 16) * 256).astype('uint8'))
    B = np.maximum(B, (b * image / (2 ** 16) * 256).astype('uint8'))
    spectre += 10

rgbArray = np.zeros((512, 512, 3), 'uint8')
rgbArray[..., 0] = R
rgbArray[..., 1] = G
rgbArray[..., 2] = B
img = Image.fromarray(rgbArray)
img.save('output.jpeg')
